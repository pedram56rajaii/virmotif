# pre-requisite
```
python 3.6+
JCpolyomavirus.fasta file, e.g. our sample file
```
# Installation guide
```
pip install -r requirements.txt
```
# Usage
```
python main.py
```
